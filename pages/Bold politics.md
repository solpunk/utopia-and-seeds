public:: true

- Seeds
	- [[Citizen assemblies]]
	- [[The New Pink Tide]]
	- [[Deliberative democracy]]
	- [[Government of Taiwan]]
- Related
	- [[Direct democracy]]