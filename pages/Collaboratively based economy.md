public:: true

- Seeds
	- [[Income sharing]]
	- [[Upstream podcast]]
	- [[Mutual credit schemes]]
	- [[Value flows]]
	- [[Robin Hood]]
	- [[Preston model]]
	-