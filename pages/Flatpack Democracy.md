public:: true

- Description
	- "Flatpack Democracy, published 2014, describes how a group of local residents in Frome, took control of their town council and set about making politics relevant, effective and fun."
	- Source: https://www.flatpackdemocracy.co.uk/books/
- Media:
	- [How Flatpack Democracy beat the old parties in the People’s Republic of Frome](https://www.theguardian.com/politics/2015/may/22/flatpack-democracy-peoples-republic-of-frome)
	- [Flatpack democracy: the new English political revolt | Anywhere but Westminster](https://www.youtube.com/watch?v=w9Ietn1EH2w)