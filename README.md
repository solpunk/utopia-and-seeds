public:: true

# Utopia and Seeds
- Published here: http://futurologi.org/utopia-seeds/#/graph
- ## Description
  A living map of utopia and its seeds. First populated from a workshop at the 2021 Solpunk gathering in Svendborg. Currently enacted in the [logseq](https://logseq.com/) knowledge base
## Structure

The knowledge graph has the following Structure

* Utopia Seeds (main node)
  * Utopian idea or vision 
      * Exisiting thing that is a seed or an actual example of this idea or vision.
          * Description
          * Links
          * etc.
- ## Explore
  Explore the knowledge base here: http://futurologi.org/utopia-seeds/#/graph
- ## Contributing
  We hope you will contribute. The simple [markdown](https://en.wikipedia.org/wiki/Markdown)-files is a public repository at https://gitlab.com/solpunk/utopia-and-seeds. Clone or download the files to your computer, edit in your text editor of choice or in the logseq application. Create Pull Request to get changes merged.
## Governance 
Solpunk has a collective governance currently living in a signal group.
## Code of conduct
There is one. Not currently written down.
## Authors and acknowledgment
Attendees at the Solpunk 2021 gathering in Svendborg. Initial idea and facilitation by zelf. Collection by cia. Graphing and initial commit by abekonge.
## License
Planetary domain